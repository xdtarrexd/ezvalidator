/**
 * Created by tarre on 2016-03-24.
 */

/**
 *
 *              Alla requirements:
 regex:/data/   mixed
 min:x       (minst x tecken, rÃ¤knar med mellanslag)
 max:x        (max x tecken, rÃ¤knar med mellanslag)
 !empty      (FÃ¥r ej vara tom, rÃ¤knar inte mellanslag)
 numbers     (Endast siffror)
 letters     (Endast bokstÃ¤ver och mellanslag)
 email       (endast "xxxxxx@xxxx.xxxxx")
 *
 */

$.fn.Validator = function (MainOptions, InputOptions) {

    this.submit(function (event) {

        var passed = true;
        var aErrors = [];

        for (var key in InputOptions) {

            var aInputOptions = InputOptions[key];

            // Requirement array
            var aRequirements = aInputOptions.requirements;
            // Element array
            var aElements = aInputOptions.elements;
            // Message string
            var sErrorMessage = aInputOptions.message;


            // Loop through all elements and check if they meet the requirements array
            var aElementCollection = [];
            var curElementsOk = true;

            if (aElements) {
                for (i = 0; i < aElements.length; i++) {
                    // Check if data id or regular id. Only for bootstrap-select support
                    var data_id = $('*[data-id=' + aElements[i] + ']')[0];

                    var curElement = document.getElementById(aElements[i]);
                    var curValue = curElement.value;

                    // Which element the indication should be on
                    if (typeof data_id == 'undefined') {
                        aElementCollection.push(curElement);
                    } else {
                        aElementCollection.push(data_id);
                    }

                    // Break if 1 or more of the elements pass all the requirements
                    if (curElementsOk && i > 0) {
                        break;
                    }

                    // 1 decides for all. same goes here.
                    var validRequirement = true;

                    for (y = 0; y < aRequirements.length; y++) {

                        var curRequirement = aRequirements[y].split(":");
                        var curRequirementId = curRequirement.shift();
                        var curRequirementParameter = curRequirement.join(':');

                        // Bail if one of the requirements is not met
                        if (!validRequirement) {
                            break;
                        }

                        switch (curRequirementId) {

                            case 'min':

                                if (curValue.length < curRequirementParameter) {
                                    validRequirement = false;
                                } else {
                                    validRequirement = true;
                                }

                                break;

                            case 'max':

                                if (curValue.length > curRequirementParameter) {
                                    validRequirement = false;
                                } else {
                                    validRequirement = true;
                                }

                                break;

                            case '!empty':

                                if (curValue.replace(/\s/g, '') == "") {
                                    validRequirement = false;
                                } else {
                                    validRequirement = true;
                                }

                                break;

                            case 'regex':

                                var pattern = new RegExp(curRequirementParameter);
                                console.log(pattern);

                                if (!pattern.test(curValue)) {
                                    validRequirement = false;
                                } else {
                                    validRequirement = true;
                                }
                                break;

                            case 'numbers':

                                var pattern = /^\d+$/;

                                if (!pattern.test(curValue)) {
                                    validRequirement = false;
                                } else {
                                    validRequirement = true;
                                }

                                break;

                            case 'letters':

                                var pattern = /^[a-z\u0080-\u00FF\s]+$/i;


                                if (!pattern.test(curValue)) {
                                    validRequirement = false;
                                } else {
                                    validRequirement = true;
                                }
                                break;

                            case 'email':

                                var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

                                if (!pattern.test(curValue)) {
                                    validRequirement = false;
                                } else {
                                    validRequirement = true;
                                }

                                break;
                        }
                    }

                    // Mimic output to CurElement is OK.
                    curElementsOk = validRequirement;

                }
            }

            // If 1 fails, everyone gets punished!
            if (!curElementsOk) {
                // We wont pass requirements
                passed = false;
                // Sett Errormessage
                aErrors.push(sErrorMessage);
                // Set border colors on elements.
                for (i = 0; i < aElementCollection.length; i++) {
                    aElementCollection[i].style.border = "1px solid #a94442";
                }
            } else {
                // Remove elements border
                for (i = 0; i < aElementCollection.length; i++) {
                    aElementCollection[i].style.border = "";
                }
            }

        }

        if (!passed) {
            return EzValidatorCreateDiv(event, MainOptions, aErrors);
        }
    });

    function EzValidatorCreateDiv(event, MainOptions, aErrors) {
        /* Cheap as DUCK */
        $("#EzValidatorBlock").remove();

        // Create H3
        var H3 = document.createElement("H3");
        H3.innerHTML = "Wops!";
        // Create Error div
        var iErrorDiv = document.createElement("div");
        iErrorDiv.id = "EzValidatorBlock";
        iErrorDiv.style = "padding: 15px;" +
            "margin-bottom: 20px;" +
            "border: 1px solid transparent;" +
            "border-radius: 4px;" +
            "color: #a94442;" +
            "background-color: #f2dede;" +
            "border-color: #ebccd1;";
        // Put h3 in Error div (Has 2 b first)
        iErrorDiv.appendChild(H3);

        // Create UL list
        var ul = document.createElement("ul");
        // Put Ul list in Error div
        iErrorDiv.appendChild(ul);


        // Fill Ul list with li Errors
        for (i = 0; i < aErrors.length; i++) {
            var li = document.createElement("li");
            li.innerHTML = aErrors[i];
            ul.appendChild(li);
        }

        // Smack that error up in da screen
        var TargetId = document.getElementById(MainOptions.targetId);
        TargetId.insertBefore(iErrorDiv, TargetId.childNodes[0]);
        // Prevent posting
        event.preventDefault();
    }
};